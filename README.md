# PROJETGITBIN1

**_**1. exercice dépôt local**_**


> Exercices Git-Gogs binom A&M


_Q:1-1 _

Pour créer un dépôt locale on doit toute d'abord créer un doccument qui sert comme répertoire de base qu'on a appelé "exercicegitgogs" à l'aide de la commande "mkdir" la syntax est alors mkdir exercicegitgogs dans le repertoire "mustapha@DESKTOP (/home)"
Dès que cela est fait on créer trois fichiers à l'aide de la commande "touch" qu'on a appelés fichier1.txt, fichier2.xl, fichier3.png.
l'étape suivante sera alors la création d'un dépôt local à l'aide de la commande "git init" cela gènere un dossier .git.
Pour éditer(écrire quelque chose) dans le fichier1.txt par exemple, on utilise la commande vim suivie du nom de fichier, à l'intérieur on passe en mode insertion cliquant "i", dès que cela est fait, en passe en mode commande en cliquant "échap", ": w" pour enregistrer et ":q" pour sortir. 
On a mis nos fichier en état de staging graçe la commande git add *.
La commande "git status" donne l'état de dépôt, nos fichiers sont en phase de statging prêt à être commités "no commit yet" voir l'image en dessous "cap1".

![](images/cap1.png )

_Q: 1-2_

Pour supprimer un fichier on utilise la commande "rm as remove" exemple "rm fichier2.txt" permet de supprimer le fichier2.txt.
La commande "mv as move" permet de renommer un fichier exemple "mv fichier1.txt binomA" le fichier1.txt va prendre le nom "binomA"
Pour vérifier qu'on a bien supprimer/renommer un fichier la commande "ll" montre l'état de notre répertoire voire l'image "cap2".

![](images/cap2.png)

_ Q : 1-3_

Pour afficher l'historique des commit on utilise la commande "git log", pour revenir dans un état précedent de dépot on utilise la commande "git show" suivie du code de commit en question voir l'image "cap3".

![](images/cap3.png)

_Q : 1-4_

Pour ajouter une étiquette à un ancien commit on utilise la commande "git tag <nom de tag>" suivie du code de commit en question, exemple : git tag v1.4 ... permet d'ajouter la tag v1.4 à notre dernier commit voir l'image en dessous "cap4":
pour vérifier l'historique des commits et le tag qu'on a ajouté on utilise la commande git log comme d'habitude.

![](images/cap4.png)

_Q:1-5_

Pour supprimer un ancien commit on utilise la commande "git reset --hard HEAD^" cette commande permet de supprimer le dernier commit qu'on a créer, la commande git log permet la vérification de la suppression de commit voire image "cap4".

![](images/cap5.png)

**_**2. exercice depot distant**_**

- -  exercice 1:- 

- Un projet est vient d'être créer sur GitLab l'image ci-dessous montre comment le créer.
![](images/cap6.png)
- Pour récupèrer un dépôt distant dans mon dépôt locale "exercicegitgogs" on utilise la commande "git clone" suivie de https de mon projet existe dans GitLab, voir l'image:
![](images/cap7.png)
- Ajouter commiter des fichiers dans projet appelé binomA; voir cap8 :
![](images/cap8.png)
- "Pusher" par la commande git push, il faut vérifier les configurations avant de pusher, c'est graçe à la commande "git pull" et vérification des modifications sur le projet GitLab binomA voir image cap9 :
![](images/cap9.png)

- - Exercice 2: -
- binome: ASMA HRIZI & MUSTAPHA MRABTI
- dans le depot crée "binomA" sur GitLab, pour ajouter un collaborateur, on procède comme l'image ci dessous montre:
![](images/cap10.png)
- voir l'image, les modifications sont bien présents dans les deux compte GitLab.
![](images/cap11.png)
- Après avoir modifier simultanémment le fichier appelé binom1.txt, on a essayé de le pusher mais cela ne marche pas "un conflit" (voir image) 
![](images/cap12.png)
Ce conflit est à résourdre manuellement par la commande "cat" soit on garde les deux modifications apportés soit on supprime un voir image:
![](images/cap13.png)

**exercice 3 Branche:**
_Q:1_ On va travailler avec le projet binoma qu'on a cloné auparavent(rappelle git clone pour cloner un depot distant).

_Q:2/ Q:3/ Q:4/Q5_ A l'aide de la commande git branch "nom de branche" ( qu'on a appllé mypart) on a genere une nouvelle branche.
 à l'aide de la commande "git branche" on peut savoir le nombre de branches qu'on a. dans ce cas on a deux branches: "main" et "mypart". Graçe à la commande "git checkout (nom de branch)" on se met sur la branche mypart, voir image: 
 ![](images/cap14.png)

On a généré une autre branche qu'on a appelée "Branch1" sous laquelle on a créer un fichier qu'on a appelé "correction.txt". On y inséré un texte et on a commité par la suite. On revient à la branche "main" et à l'aide de la commande "git merge Branch1" on a fusionné les deux branches."git push --set-upstream origin Branch1" pour envoyer la nouvelle branche sur le serveure et on canstate bien que les modifications sont prises en compte, voir image:
  ![](images/cap19.png)
  ![](images/cap20.png)
  ![](images/cap21.png)
  
Pour supprimer "Branch1" on a utilisé la commande "git Branch1 -d"


** Exercice Fork**
Exercice1: 
On a créé un dépôt public nommé "HA" voir image ![](images/cap22.png)
Rq: la pull request n'est pas autorisé .

Exercice2: 
_Q1/Q2_ On a créé un dépôt privé qu'on a appelé "githb" et un dépôt public appelé "HA".
voir image ![](images/cap23.png)


_Q3_: Dans le dépôt privé githb on a créé une branche "public 2" , à partir du quelle on a ajouté un fichier branch2 qu'on a commmité ensuite et qu'on a fusionner avec la branch main de notre dépôt, avec git push on récupère notre branch avec les modifications dedons, pour pusher cette branch "public2" dans le dépôt public HA, on a utilier la commande de fork "git push -u myforkRp public2" le myforkRp est un répertoir locale qu'on a créé dans notre dépôt distant privé. voir image: 
![](images/cap28.png) 

_Q4/Q5:_
Dans le projet githb, on doit merger la branch main avec la branch public pour voir et pouvoir récupérer l'ensemble des modifications qui ont été faites. voir image code et résultat (nou avons apporté des modifications sur le fichier myfork00):
![](images/cap29.png) 

